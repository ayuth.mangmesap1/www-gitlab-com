---
layout: handbook-page-toc
title: Secure Technical Documentation
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Architecture

- [Overview](./overview/)
- [Severity Levels](./severity-levels/)
- [Feedback](./feedback/)(Dismiss, create an issue or a Merge Request)

## Researches

- [Data model for dependencies information](./data-model-for-dependencies-information/)

## Brown bag sessions

Secure team members also share knowledge through [brown bag sessions](https://gitlab.com/gitlab-org/secure/brown-bag-sessions#brown-bag-sessions) on various topics.
