---
layout: handbook-page-toc
title: "Data Driven Talent Acquisition"
description: "Data Driven Talent Acquisition is defined as focusing on fewer people that have more aptitude and connection to the company."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Data Driven Talent Acquisition

Dave Gilbert, [Sid Sijbrandij](https://about.gitlab.com/company/team/#sytses) and Brendan Browne discuss how to generate a Data Driven Talent Acquisition Model.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8qNxeEJimpU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Data Driven Talent Acquisition at GitLab

Data Driven Talent Acquisition (DDR) is defined as focusing on fewer people that have more aptitude and connection to the company. This approach helps to optimize the entire hiring process by leveraging the data on the prospects that are more likely to respond to our outreach and are more likely to be successful throughout the interviewing process.

Data Driven Talent Acquisition approach presents us with the specific data collected on the prospects and helps to gain a deeper understanding of our target talent pools. It also helps the Talent Acquisition team to strategize our sourcing effort by prioritizing quality of sourced prospects over their quantity.

As we aim to build an even more diverse team in the most efficient way possible, while also sustaining our culture, we've created a list parameters that could be considered when building the sourcing strategy for a particular opening, and this list keeps growing:
1. Affinity and engagement with GitLab Talent Brand
1. Interest in our current or past openings
1. Skillset alignment
1. [Preferred companies](https://about.gitlab.com/handbook/hiring/preferred-companies/)
1. Company connections
1. Longevity
1. Potential contribution to GitLab's diversity
1. Contribution to GitLab's efficiency

GitLab Talent Acquisition team also strives to leverage the use of [LinkedIn _Talent Insights_ reports](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/req-overview/#step-3-complete-kick-off-session-agree-on-priority-level--complete-a-sourcing-session). Given specific inputs (e.g. job titles, skills, and/or geographic regions) we're able to see how big a talent pool is. That talent pool can be honed in further by referencing how many of those prospects are either engaged with GitLab updates, have followed or visited GitLab's LinkedIn profile, or have viewed posted jobs.

### General guidelines
While there are different ways of combining the parameters listed above, our goal is to use Data Driven Recruitment approach at the beginning of every search to build robust pipeline and oprtimize our sourcing effort. These are the steps you can take to adopt the Data Driven Approach to your search:

1. Define your hiring needs and the purpose of the DDR to understand what results you’d like to get.
1. Break down the project into steps, for example:
- Defining cues and segmenting the market;
- Sourcing and outreach;
- Talent Acquisition.
3. Segment the market based on different cues you are interested in collecting the data on and proceed to sourcing and talent acquisition.

### Resources
To learn more about Data Driven Talent Acquisition at GitLab you can check out the [DDR training](https://docs.google.com/presentation/d/1hsjYPtHtwiOEe6eBZvbudnafs3UbpvEU4B8CYspHC6s/edit#) and the first [DDR pilot project](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/363) (kindly note that these resources are available to GitLab team members only). If you have any questions regarding this approach and how to implement it, please reach out to your [Sourcing Partner](https://about.gitlab.com/handbook/hiring/recruiting-alignment/) or the Sourcing team in the #sourcing-hacks Slack channel.
