---
layout: job_family_page
title: "Board Member"
---

GitLab Board Members are accountable to GitLab shareholders.

## Responsibilities

* Monitor the performance of the [CEO](/handbook/ceo/) and other [Executive Officers](/company/team/structure/#executives)
* Give feedback on the [mission](/company/mission/#mission), [values](/company/mission/#values), and [strategy](/company/mission/#goals)
* Ensure adequate resources are available to achieve our goals and that those resources are used effectively.
* Advocate for GitLab externally.
* Participate in [board meetings](/handbook/board-meetings/)
* Participate in [board committees](/handbook/board-meetings/#board-and-committee-composition)
* Help find, evaluate, and close executive hires.
* Do an [Ask Me Anything (AMA)](/handbook/communication/#company-call)

## Requirements

* Executive role at a public company or lead investor in GitLab
* Ability to use GitLab

## Performance Indicators

Board members will have a yearly review of their performance. 
