---
layout: job_family_page
title: Digital Experience Management
---
The Digital Experience Mangement Job Family sees the team as their product. While they are credible as digital product leaders and know the details of what Product Designers and Engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of digital marketing commitments and are always looking to improve productivity. They must also coordinate cross-functionally to accomplish collaborative goals.

## levels

## Manager, Digital Experience

The Manager, Digital Experience reports to the Senior Manager, Digital Experience, Director of Digital Experience or the Senior Director, Inbound Marketing. The Product Manager (Marketing) and Technical Product Owner (Marketing) report to the Manager, Digital Experience.

#### Manager, Digital Experience Job Grade

The Manager, Digital Experience is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Digital Experience Responsibilities

* **Product knowledge:** Understand the features and value of the end-to-end GitLab product.
* **Cross-product collaboration:** Proactively identify large, strategic digital marketing opportunities that span the Marketing team. Work with other cross-functional Managers to drive Marketing initiatives.
* **Deliverables:** Review deliverables (sprint plans, KRs, research, designs, etc.) that your team creates, and provide feedback to ensure high-quality output.
* **Research:** Identify strategic user research initiatives and work with cross-functional Managers to organize research efforts.
* **UX evangelism:** Communicate the value of UX to cross-functional GitLab team members, and influence the teams you support to prioritize UX initiatives.
* **UX process**: Set up and manage collaborative processes within your team to ensure Product Designers, Data Scientists, and Researchers are actively working together. Make sure everyone has exposure to the work that is happening within the broader team.
* **Team building:** Collaborate with Product Manager and Technical Product Owner to hire a world-class team of Product Designers, Engineers, Researchers, and Data Scientists.
* **Public presence:** Engage in social media efforts, including writing blog articles and responding on Twitter, as appropriate.
* **Vision and direction:** Have an awareness of strategy, and vision of the Marketing digital experience
* **People management:** Hold regular 1:1s with every member of your team and create Individual Growth Plans with monthly check-ins.

#### Manager, Digital Experience Requirements

* A minimum of 3 years experience managing a group of designers and engineers.
* Ability to write code (HTML, CSS, JS)
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy and defining monthly OKRs based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group).
* Ability to use GitLab.

### Senior Manager, Digital Experience 

The Senior Manager, Digital Experience reports to the Senior Manager, Digital Experience, Director of Digital Experience or the Senior Director, Inbound Marketing. The Product Manager (Marketing) and Technical Product Owner (Marketing) report to the Manager, Digital Experience.

#### Senior Manager, Digital Experience Job Grade 

The Senior Manager, Digital Experience is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Digital Experience  Responsibilities

* Extends that of the the Manager, Digital Experience responsibilities.
* Collaborating with cross-functional leaders to define quarterly OKRs that support Marketing's goals.
* Organizational design to increase collaboration and efficiency.
* Engaging with Finance teams to contribute to forecasting and reporting on business performance.
* Conducting skip level 1:1s every quarter with the [Digital Experience team](/handbook/marketing/inbound-marketing/digital-experience/)
* Pro-actively connecting with cross-functional GitLab team members to identify opportunities.
* Demonstrating GitLab's CREDIT values and modelling GitLab's Leadership traits.

#### Senior Manager, Digital Experience Requirements

* Extends the Manager, Digital Experience requirements.
* A minimum of 6 years experience managing a group of designers and engineers.
* Advanced level of HTML, CSS, and JS.
* Experience as agile project manager a plus.
  
### Director of Digital Experience

The Director of Digital Experience reports to the Senior Manager, Digital Experience, Director of Digital Experience or the Senior Director, Inbound Marketing. The Technical Product Owner and Product Manager (Marketing) report to the Director of Digital Experience.

#### Director of Digital Experience Job Grade

The Director of Digital Experience is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director of Digital Experience Responsibilities

* Extends the Senior Manager, Digital Experience responsibilities.
* **Performance tracking:** Define and manage performance indicators for the Digital Experience team by independently managing Inbound Marketing KPIs.
* **Cross-product collaboration:** Actively advocate for Digital Experience throughout the organization by ensuring Digital Experience responsibilities are reflected in the Marketing organization.
* **Product knowledge:** Help drive cross-functional workflows by having an awareness of what's happening across all Marketing teams through active participation in reviews, showcases, and Group Conversations.
* **Goal setting:** Independently manage the creation and execution of Digital Experience OKRs with feedback from the Inbound Marketing team and Marketing Leadership.
* **UX evangelism:** Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* **Design strategy:** Communicate significant Digital Experience design strategy decisions to leadership and the wider company.
* **People management:** Coach Product Manager (Marketing) and Technical Product Owner (Marketing) on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* **Skip levels:** Conduct quarterly skip levels with your reports' direct reports.
* **Vision and direction**: Have an awareness of opportunities, strategy, and visions across Marketing. 
* **Team building:** Hire and retain a world-class team.

#### Director of Digital Experience Requirements

* Extends the Senior Manager, Digital Experience requirements.
* A minimum of 10 years experience managing engineers, designers, researchers, data scientists, product managers, and product owners and leading digital experience for a SaaS company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Solid technical understanding of code (HTML, CSS, JS) and frameworks (JAMstack) as well as design systems (Storybook).
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)

## Performance Indicators

* [Contributing to the success of Marketing's Quarterly Initiatives](/handbook/marketing/inbound-marketing/#q3-fy21-initiatives)
* [Identifying and organizing epics into executable Sprint plans](/handbook/marketing/inbound-marketing/brand-and-digital-design/#sprint-planning)
* [Successfully completing weekly Sprint tasks](/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-cycle)
* [Collaborating on identifying issues to be completed within Epics](/handbook/marketing/inbound-marketing/#epics)

## Career Ladder

The Digital Experience Management job family career ladder is yet to be defined.

## Hiring Process
 
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, you can find their job title on our [team page](/company/team).
 
* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with the Senior Director, Inbound Marketing.
* Next, candidates will be invited to schedule a 30 minute interview with a Fullstack Engineer from the Digital Experience team.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Digital Experience.
* Next, candidates will be invited to schedule a 30 minute interview with our CMO.
* Finally, candidates will be invited to schedule a 30 minute follow up interview with the Senior Director, Inbound Marketing.
 
Additional details about our process can be found on our [hiring page](/handbook/hiring).
