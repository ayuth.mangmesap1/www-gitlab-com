---
layout: security
title: Security - Frequently Asked Questions
description: "We designed this Frequently Asked Questions page to serve as a starting point for those interested in GitLab's Security."
canonical_path: "/security/faq/"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Security Frequently Asked Questions

At GitLab, security is a priority. But so is transparency. And that's why it is so important that our Customers and Prospects have the most accurate information regarding our security. We designed this Frequently Asked Questions page to serve as a starting point for those interested in GitLab's Security. For more in depth resources, please visit our [Customer Assurance Package](https://about.gitlab.com/security/cap/). 

<details>
  <summary markdown="span">
  Cloud Questions
  </summary>
  
<details>
  <summary markdown="span">
  **Q - What is the difference between SaaS and self-hosted?**
 </summary>

 A - GitLab has two deployment models. The first is a multi-tenant public cloud Software as a Service (SaaS). For customers who utilize the SaaS model, GitLab.com, GitLab deploys, operates, monitors, and maintains the instance continuously. The second is Self-Hosted. With this method, the customer is responsible for deploying, operating, monitoring and maintaining GitLab software on their own infrastructure. 
	    
</details>
 <details>
   <summary markdown="span">
 **Q - What does multi-tenant, public cloud, and Software as a Service mean?**
   </summary>
	            
 A - These are cloud computing terms defined in the [National Institute of Standards and Technology Special Publication 800-145: The NIST Definition of Cloud Computing](https://csrc.nist.gov/publications/detail/sp/800-145/final). To summarize, **Multi-Tenant** refers to the fact that multiple entities are deployed on the same underlying Infrastructure. **Public Cloud** refers to the fact that GitLab.com is open to most users with an internet connection with the few exceptions noted in our [Terms of Service](https://about.gitlab.com/terms/#subscription)
	    
</details>
 <details>
   <summary markdown="span">
**Q - Does GitLab.com depend on any other cloud providers?**
 </summary>

A - Yes, GitLab.com is deployed on [Google Cloud Platform (GCP)](https://cloud.google.com/security) Infrastructure as a Service (IaaS). For detailed architecture please see our [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/)
	    
</details>
<details>
  <summary markdown="span">
 **Q - What deployment model does GitLab recommend?**
 </summary>

 A - There is no one-size fits all solution, however for most entities GitLab.com is the most efficient and secure way to make use of all the great features GitLab offers. GitLab.com comes preconfigured and running and it takes minimal internal resources to support. Some entities with highly specific needs may choose to deploy GitLab as a self-hosted software to tailor GitLab to their needs. If you need help determining which is right for your organization [our Sales team would be happy to help](https://about.gitlab.com/sales/) evaluate your needs.
	  
</details>
 <details>
<summary markdown="span">
 **Q - What is Shared Responsibility?**
</summary>

A - In a cloud computing environment it is important to understand the roles and responsibilities all parties have in keeping data secure. To learn more about [Shared Responsibilities at GitLab](#)<!--todo: link whitepaper on shared responsibility--> see our whitepaper.
	    
</details>

</details>

<details>
  <summary markdown="span">
	    Security Questions
	    </summary>

<details>
  <summary markdown="span">
 **Q - Does GitLab have a Security Team?**
   </summary>

A - Yes; GitLab has a dedicated [Security Department](https://about.gitlab.com/handbook/engineering/security/). The current individual makeup of our security team can be seen on our [About the Team Page](https://about.gitlab.com/company/team/?department=security-department)
	 
 </details>
 <details>
  <summary markdown="span">
 **Q - Does GitLab have an Information Security Program?**
  </summary>

A - Yes; GitLab has a robust information security program and [many of our policies and procedures](https://about.gitlab.com/handbook/engineering/security/#resources) are publicly accessible.
	   
</details>
<details>
  <summary markdown="span">
 **Q - Is GitLab's Security Program aligned with industry standards?**
 </summary>

 A - Yes. GitLab's [Security Compliance Controls](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html) are publicly viewable. The GitLab Control Framework forms the building blocks of our Security Compliance Program.
	   
</details>
<details>
  <summary markdown="span">
**Q - Does GitLab hold any 3rd Party Compliance Attestations?**
  </summary>

A - Yes; GitLab currently has a SOC2 Type 2 Report that [can be provided under NDA](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/certifications.html). GitLab has a publicly available SOC 3 that [can be found here](https://gitlab.com/gitlab-com/gl-security/soc-3-project).
	    
</details>
<details>
  <summary markdown="span">
 **Q - Does GitLab encrypt my data? At Rest? In Transit?**
  </summary>

A - Yes, Yes, and Yes. GitLab takes the security of data our customers entrust us with very seriously. Data is Encrypted at Rest using Google Cloud Platform's Encryption at Rest with the AES-256 Algorithm. All data in-transit is encrypted using TLS 1.2.

</details>
<details>
  <summary markdown="span">
 **Q - Why do I need to keep my GitLab instance updated?**
  </summary>

A - We’re transparent about our security issues and, where possible, vulnerability details are made public 30 days after the release in which they were patched. We want our users to be informed and also to secure their instances through regular updates.  **Users can be notified about new security releases though our [security notices mailing list](/company/contact/), the [security release RSS feed](/security-releases.xml) or the [RSS feed for all GitLab releases](/all-releases.xml).** For more information about our security releases and process, see our FAQ entry “How does GitLab handle security releases?”.

We understand that updating software involves time and resources. However, the truth is, any widely used software will have continuously discovered vulnerabilities and any software that is not continually updated, can be vulnerable.  

**As part of maintaining good security hygiene we urge our customers to keep their instances updated. We continue to be dedicated to ensuring all aspects of GitLab, and our methods for handling customer data, are held to the highest security standards.** 

For more tips on keeping your GitLab instance secure, read ["GitLab instance: security best practices"](https://about.gitlab.com/blog/2020/05/20/gitlab-instance-security-best-practices/).

</details>
<details>
  <summary markdown="span">
 **Q - Does GitLab have an incident response plan?**
 </summary>

A - Yes; You can view our [Incident Response Guide](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/sec-incident-response.html) in the Handbook.
	    
 </details>
 <details>
   <summary markdown="span">
**Q - Does GitLab regularly undergo penetration testing by a 3rd party firm?**
  </summary>

A - Yes. Our 3rd Party Penetration Test Summary can be provided under NDA.
	   
</details>

<details>
  <summary markdown="span">
**Q - Does Gitlab have a business continuity plan/disaster recovery plan?** 
  </summary>

A - Yes. GitLab, Inc's [Business Continuity and Redundancy plans](https://about.gitlab.com/handbook/business-ops/gitlab-business-continuity-plan/) is available in the Handbook for our SaaS customers. 

</details>

<details>
  <summary markdown="span">
 **Q - What data does GitLab have access to (personal and business)?**
  </summary>

A - GitLab is the Data Processor and our Customers are Data Controllers. GitLab collects information required to set up your GitLab.com account. Please see our DPA on the Privacy Compliance page. 
<br>
Personal Details (including but not limited to):
<br>
- First Name
- Last Name
- Email
- City
- Postal Code
- Country
- State or Territory
- Company/Group Details (Including but not limited to)
- Company Name
- Total # of employees

</details>
<details>
<summary markdown="span">
**Q - How does GitLab handle security releases?**
</summary>
A - GitLab releases patches for vulnerabilities in dedicated security releases.
There are two types of security releases: a monthly, scheduled security 
release, that is targeted to go out a week after the feature release (which deploys on the 22nd of each month),
and ad-hoc security releases for critical vulnerabilities. The fix for every
vulnerability is backported to the current release, and the 2 previous
`major.minor` versions.  

To help you understand the vulnerabilities that were fixed, a blog post accompanies
each security release and includes a brief description, the 
versions affected, and the assigned CVE ID for each vulnerability. 
Feature and security release blog posts are located in the [`releases` section of our blog](https://about.gitlab.com/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public 30 days 
after the release in which they were patched. It is highly recommended that 
all customers upgrade to at least the latest security release for their 
supported version.

To be notified when a security release is released, the following channels are
available:
- [Security Notices mailing list](/company/contact/)
- [Security Release RSS feed](/security-releases.xml)
- [All GitLab Releases RSS feed](https://about.gitlab.com/all-releases.xml)
</details>

</details>
<details>
<summary markdown="span">
GitLab's Access to Your Private Repositories
</summary>


<details>
  <summary markdown="span">
 **Q - What access does GitLab have to private repositories?**
</summary>

A - GitLab Support will not access private repositories unless required for support reasons, meaning for account and ownership verification, and when requested by the owner of the repository via a support ticket. Forms of access include, but are not limited to, [impersonation](https://docs.gitlab.com/ee/user/admin_area/#user-impersonation). When working on a support issue, we strive to respect your privacy as much as possible. Once we've [verified account ownership](/handbook/support/workflows/account_verification.html), we will only access the files and settings needed to resolve your issue. Support may sign into your account to access configurations but we will limit the scope of our review to the minimum access required to solve your issue. In the event we need to pull a clone of your code, we will only do so with your consent. All cloned repositories are deleted as soon as the support issue has been resolved. There are two exceptions to this policy on accessing private repositories: we are investigating a suspected violation of our [terms of service](/terms/) or we are compelled to access repositories as part of a legal or security matter.
</details>

</details>

## Getting More information
- The [Customer Assurance Package](https://about.gitlab.com/security/cap/) was designed to allow the anyone to review and evaluate GitLab's security posture. 

This FAQ applies solely to our Software as a Service (SaaS); GitLab.com.

