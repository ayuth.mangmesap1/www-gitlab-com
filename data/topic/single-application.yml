description: Learn how simplifying your toolchain using a single application can
  increase your speed while reducing cost and risk.
canonical_path: /topics/single-application/
file_name: single-application
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: Why a single application for DevOps?
header_body: >-
  Learn how simplifying your toolchain using a single application can increase
  your speed while reducing cost and risk.


  [Try GitLab for Free →](/free-trial/)
body: >+
  ## The DevOps toolchain tax


  Traditional DevOps solutions require cobbling together multiple tools that were never designed to work together in order to build an DevOps toolchain.


  ![DevOps toolchain complexity](/images/topics/devops-toolchain-complexity.png)


  This leads to having to pay a "tax" on your toolchain made up hidden costs.


  * Time and cost to acquire point tools

  * Time and cost to integrate all of these tools

  * Time and cost to train users on many tools

  * User context switching between all of these tools

  * Time and cost to administer all of these tools


  ## Single application architecture


  GitLab is a complete DevOps platform designed from the ground up as a single application. From project planning and source code management, to CI/CD, security, and monitoring GitLab’s capabilities are built-in as part of the app so you don’t have to integrate multiple tools.


  ![Single DevOps application](/images/topics/single-application-all-devops.png)


  ## Benefits of a single application


  <iframe width="560" height="315" src="https://www.youtube.com/embed/MNxkyLrA5Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


  ### Reduce risk with a single set of permissions


  Rather than having to manage authentication and authorization across many tools. GitLab has a single login and one place to set permissions so everyone has the correct access.


  ### Reduce costs with less administrative overhead


  With a single application to install, configure, and maintain there’s less administrative overhead. Since fewer staff needed to administer a single application verse a complex toolchain more of your engineering resources can be allocated towards development of features for your users.


  ### Increase speed with a lower time to resolution


  When a build pipeline fails how do you troubleshoot? Is it a problem with the infrastructure or did new code fail a test? Perhaps there is state in the original specification that needed to help debug. With traditional toolchains the issue tracker, code repository, and CI/CD pipeline are all separate tools. When teams need to troubleshoot they have to pass state back and forth in a ticket because they likely don’t all have access to the same applications. 


  With GitLab, everyone who needs to help troubleshoot a failure has access to all of the data. Pipeline, code, comments, issues, and test results all appear on the merge request so there’s a single view. With everyone on the same page troubleshooting is much simpler and things get up and running faster.


  [See more benefits of a single application →](/handbook/product/single-application/)
